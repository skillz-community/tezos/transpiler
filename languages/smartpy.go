package languages

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

// Smartpy Struct (Model)
type Smartpy struct {
	Data      string `json:"data"`
	ClassName string `json:"className"`
}

func (smp *Smartpy) Transpile() ([]byte, error) {
	smp.ClassName += "()"
	tmpfile, err := ioutil.TempDir("tmp", "skillz_*")
	defer os.RemoveAll(tmpfile)

	inputFile := tmpfile + "/tmp.smpy"

	if err != nil {
		return nil, fmt.Errorf("error creating temporary directory: %w", err)
	}
	err = ioutil.WriteFile(inputFile, []byte(smp.Data), os.ModePerm)
	if err != nil {
		return nil, fmt.Errorf("error writing file: %w", err)
	}
	_, err = exec.Command("./smartpy-cli/SmartPy.sh", "compile", inputFile, smp.ClassName, tmpfile).Output()
	if err != nil {
		return nil, fmt.Errorf("error during compilation: %w", err)
	}
	file, _ := os.Open(filepath.Join(inputFile[:len(inputFile)-5] + "_compiled.tz"))
	defer file.Close()
	michelson, err :=  ioutil.ReadAll(file)
	if err != nil {
		return  nil, fmt.Errorf("Error reading file: %w", err)
	}
	return michelson, nil
}