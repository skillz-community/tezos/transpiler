module transpiler

go 1.15

require (
	github.com/gorilla/handlers v1.5.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.7.0
)
