Please before using our transpiler pay attention to:
- Change EXEMPLE_FRONT_ENDPOINT to your front' endpoint in main.go
- Change EXEMPLE_TRANSPILER_ENDPOINT to your listening port in main.go

Once those steps done, you only have to launch the dockerfile.