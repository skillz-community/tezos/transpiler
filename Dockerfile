FROM golang as go-builder

ARG SOURCE_DIR

ADD ./app/
WORKDIR /app

RUN go build -o /tmp/transpiler

FROM alpine as smp-builder

RUN apk add --no-cache curl bash npm python3
RUN bash -c "(echo y; echo y) | sh <(curl -s https://smartpy.io/dev/cli/SmartPy.sh) local-install-auto"

FROM node

WORKDIR /app

COPY --from=go-builder /tmp/transpiler /app/transpiler
COPY --from=smp-builder /root/smartpy-cli /app/smartpy-cli

RUN ls /app

EXPOSE 8083

CMD /app/transpiler