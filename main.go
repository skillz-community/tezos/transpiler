package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"transpiler/languages"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type Michelson struct {
	Error     string `json:"error"`
	Michelson string `json:"michelson"`
}

func transSmartpyToMichelson(w http.ResponseWriter, r *http.Request) {
	var smp = &languages.Smartpy{}

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(data, &smp)
	var ml Michelson
	tzData, err := smp.Transpile()
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		ml.Michelson = ""
		ml.Error = err.Error()
	} else {
		w.WriteHeader(http.StatusOK)
		ml.Michelson = string(tzData)
		ml.Error = ""
	}
	w.Header().Set("Content-type", "application/json")
	err = json.NewEncoder(w).Encode(ml)
	if err != nil {
		fmt.Println(err)
	}
}

func isAlive(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func handleRequest() {
	muxRouter := mux.NewRouter().StrictSlash(true)
	corsData := cors.New(cors.Options{
		AllowedOrigins: []string{os.Getenv("EXEMPLE_FRONT_ENDPOINT")}, // Change EXEMPLE_FRONT_ENDPOINT to your front' endpoint
	})
	muxRouter.HandleFunc("/trans/smartpy", transSmartpyToMichelson).Methods("POST")
	muxRouter.HandleFunc("/health", isAlive).Methods("GET")

	handler := corsData.Handler(muxRouter)
	log.Fatal(http.ListenAndServe(os.Getenv("EXEMPLE_TRANSPILER_ENDPOINT"), handler)) // Change EXEMPLE_TRANSPILER_ENDPOINT to your listening port
}

func main() {
	os.Mkdir("tmp", os.ModePerm)
	handleRequest()
}
